#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QSerialPort *Serial;
    QSerialPortInfo *SerialInfo;

    void UpdateSerialInfo();

public slots:
    void ReadyRead();

private slots:
    void on_actionStart_triggered();

    void on_actionStop_triggered();

    void on_comRefresh_clicked();

    void on_errorButton_clicked();

private:
    Ui::MainWindow *ui;
    QByteArray buffer;
};

#endif // MAINWINDOW_H
