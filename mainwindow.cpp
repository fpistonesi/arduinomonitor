#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::MSWindowsFixedSizeDialogHint);

    SerialInfo = new QSerialPortInfo;
    this->UpdateSerialInfo();

    Serial = new QSerialPort;
    connect(Serial, SIGNAL(readyRead()), this, SLOT(ReadyRead()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::UpdateSerialInfo()
{
    ui->comboPort->clear();

    QStringList pName;
    if(SerialInfo->availablePorts().count() < 1)
    {
        ui->actionStart->setEnabled(false);
        return;
    }
    for(int i = 0; i < SerialInfo->availablePorts().count(); i++)
    {
        pName << SerialInfo->availablePorts().at(i).portName()
                 +" " + SerialInfo->availablePorts().at(i).description();
    }
    ui->comboPort->addItems(pName);

    ui->actionStart->setEnabled(true);
}

void MainWindow::ReadyRead()
{

    while(Serial->canReadLine())
    {
        buffer = Serial->readLine();
        buffer.trimmed();
    }

    QStringList values = QString(buffer).split(" ");

    if(values.count() < 12) return;
    for(int i = 0; i < 12; i++)
    {
        this->findChild<QCheckBox*>(QString("dValue_%1").arg(i+2))->setChecked((bool)values.at(i).toInt());
    }

    for(int i = 0; i <= 5; i++)
    {
        this->findChild<QProgressBar*>(QString("aValue_%1").arg(i))->setValue(values.at(i+12).toInt());
    }

    return;
}

void MainWindow::on_actionStart_triggered()
{
    QString pDescription = SerialInfo->availablePorts().at(ui->comboPort->currentIndex()).description();
    QString boud = QString::number(Serial->baudRate());
    QString data = " data:" + QString::number(Serial->dataBits());
    QString parity = " parity:" + QString::number(Serial->parity());
    QString stop = " stop: " + QString::number(Serial->stopBits());
    QString cfg = data + parity + stop;

    QString pName = SerialInfo->availablePorts().at(ui->comboPort->currentIndex()).portName();
    Serial->setPortName(pName);
    Serial->setBaudRate(ui->comboBoud->currentText().toInt());

    if(Serial->open(QIODevice::ReadWrite))
    {
        ui->messagePane->setText(pDescription + " " + pName + " " + boud + cfg);
    }
    else
    {
        ui->stackedWidget->setCurrentIndex(1);
        ui->errorLabel->setText("COM error");
        return;
    }
    ui->actionStart->setEnabled(false);
    ui->actionStop->setEnabled(true);
}

void MainWindow::on_actionStop_triggered()
{
    Serial->close();
    ui->actionStop->setEnabled(false);
    this->UpdateSerialInfo();
}

void MainWindow::on_comRefresh_clicked()
{
    this->UpdateSerialInfo();
}

void MainWindow::on_errorButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}
