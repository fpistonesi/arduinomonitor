#-------------------------------------------------
#
# Project created by QtCreator 2015-04-25T18:53:13
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ArduinoMonitor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc

win32:RC_ICONS += arduino-monitor.ico
VERSION = 0.1.1
VER_MAJ = 0
VER_MIN = 1

